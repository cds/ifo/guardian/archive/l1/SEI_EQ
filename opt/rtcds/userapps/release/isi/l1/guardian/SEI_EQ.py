#!/usr/bin/env python
from guardian import GuardState, GuardStateDecorator
from guardian import NodeManager
import cdsutils
import subprocess
import time
import numpy as np
import gpstime

brs_chambers = [['ITMX','X'],['ITMY','Y'],['ETMX','X'],['ETMY','Y']]
blrms_chambers = ['ITMX','ITMY','ETMX','ETMY']
ramp_senscor_time = 60
bsc_chambers = ['ITMX','ITMY','BS','ETMX','ETMY']
ham_chambers = ['HAM1','HAM2','HAM3','HAM4','HAM5','HAM6']
fc_ham_chambers = ['HAM7','HAM8']
corner_chambers = ['HAM1','HAM2','HAM3','HAM4','HAM5','HAM6','ITMX','ITMY','BS']
end_bsc_chambers = ['ETMX','ETMY']


# fixed EQ_THRESHOLD scale as now in microns/sec - K Thorne 8/11/2022
EQ_THRESHOLD =0.95
EQ_UP_RMS_THRESHOLD = 210
EQ_DOWN_RMS_THRESHOLD = 410 #TODO: review this number was 180 TJ changed to 260 to be just above isc lock
PEAKMON_THRESHOLD = 400
PICKET_THRESHOLD = 400
PICKET_FENCE_THRESHOLD = 1600
PICKET_FENCE_GLITCH = 10000
low_noise = 2000
down = 10
DAMPED_THRESHOLD = 5000
earthquake_timer = 25*60 # This is the time we wait to check the ground motion amplitude after entering the EQ_HERE state

# linear predictor parameters
EARTH_RADIUS = 6371 #km
B1 = 2.6126
B2 = -2.0992
B3 = -0.1917
B4 = -19.1342
B5 = 0.3299

nodes = NodeManager([
    'SEI_CONFIG',
])

nominal = 'EQ_MONITOR'
request = 'IDLE'

#########################################
### DECORATORS

class lock_loss_dec(GuardStateDecorator):
    def pre_exec(self):
        if ezca['GRD-ISC_LOCK_STATE_N'] < low_noise:
            return 'DOWN'


class linear_model_velocity(GuardStateDecorator):
    def pre_exec(self):
        for seismon in range(0,5): 
            mag = ezca['SEI-SEISMON_EQ_MAGNITUDE_MMS_' + str(seismon)] #richter scale
            dist = ezca['SEI-SEISMON_EQ_LLO_DISTANCE_M_' + str(seismon)] #km
            depth = ezca['SEI-SEISMON_EQ_DEPTH_KM_' + str(seismon)] #km
            #predicted_motion = np.exp(2.25*mag - 1.54*np.log(dist/EARTH_RADIUS/np.pi) - 0.22*np.log(depth/EARTH_RADIUS) - 16.29) #[um/s] OLD version
            #predicted_motion = np.exp(B1*mag + B2*np.log(dist/EARTH_RADIUS/np.pi) + B3*np.log(depth/EARTH_RADIUS) + B4) + B5 #[um/s] Newer version with offset
            predicted_motion = ezca['SEI-SEISMON_LLO_LINEAR_MODEL_VELOCITY_' + str(seismon)]



#########################################
### FUNCTIONS

def sc_chan_name(chamber, dof):
    if 'HAM' in chamber:
        if 'HAM1' in chamber:
            sei = 'HPI'
            senscor = 'SENSCOR'
        else:
            sei = 'ISI'
            senscor = 'SENSCOR'
    else:
        if 'ETMY' in chamber and 'Z' in dof:
            sei = 'HPI'
            senscor = 'SENSCOR'
        else:
            sei = 'ISI'
            senscor = 'ST1_SENSCOR'
    channel_name = sei + '-' + chamber + '_' + senscor + '_' + dof
    return channel_name


def check_senscor_state(chamber,dof,next_chan):

    a = ezca[sc_chan_name(chamber, dof) + '_FADE_CUR_CHAN_MON']
    if a!=next_chan:
        return False
    else:
        return True


def in_earthquake_mode():
    earthquake_mode = True
    #Check requested filters here, return True if earthquake mode has been requested
    for chamber in ham_chambers+bsc_chambers:
        for dof in ['X','Y','Z']:
            earthquake_mode = earthquake_mode and check_senscor_state(chamber,dof,5)
    return earthquake_mode

    
def monitor_seismon_new(max_wait_time=-6): 
    # Verify the last 5 earthquakes updated by linear predictor, if any of them were above threshold and update the boolean
    above_seismon_bomb_cond = False
    for seismon in range(0,5): 
        predicted_motion = ezca['SEI-SEISMON_LLO_LINEAR_MODEL_VELOCITY_' + str(seismon)] #[um/s] 
        arrival_time = ezca['SEI-SEISMON_LLO_R35_ARRIVALTIME_MINS_' + str(seismon)]
        above_seismon_bomb_cond = above_seismon_bomb_cond or (predicted_motion >= EQ_THRESHOLD and arrival_time > max_wait_time)
        if above_seismon_bomb_cond:
            return True, seismon  
    return above_seismon_bomb_cond, -1


def monitor_seismon():
    # Verify the last 5 earthquakes updated by seismon, if any of them were above threshold and update the boolean
    above_seismon_bomb_cond = False
    for seismon in range(0,5): 
        predicted_motion = ezca['SEI-SEISMON_LLO_R35_VELOCITY_MPS_' + str(seismon)]
        arrival_time = ezca['SEI-SEISMON_LLO_R35_ARRIVALTIME_MINS_' + str(seismon)]
        above_seismon_bomb_cond = above_seismon_bomb_cond or (predicted_motion >= EQ_THRESHOLD and arrival_time > -6)
        if above_seismon_bomb_cond:
            return True, seismon  
    return above_seismon_bomb_cond, -1


def is_big_earthquake_arriving(): 
    # Define a boolean to check if local ground 30-100mHz Z BLRMS motion is higher than threshold 
    above_z_RMS_threshold_UP = True

    # Check for every STS (IX, IY, EX, EY) and make boolean = True if condition is met for ALL
    for chamber in blrms_chambers:
        above_z_RMS_threshold_UP = above_z_RMS_threshold_UP and (ezca['ISI-GND_STS_' + chamber + '_Z_BLRMS_30M_100M'] > EQ_UP_RMS_THRESHOLD)

    above_z_peak_threshold = (ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'] > PEAKMON_THRESHOLD)

    #EQ_cond = monitor_seismon_new() and above_z_peak_threshold #and above_z_RMS_threshold_UP #for LM prediction version instead of SEISMON
    EQ_cond = monitor_seismon() and above_z_peak_threshold #and above_z_RMS_threshold_UP

    return EQ_cond


def has_earthquake_passed():
# Define a boolean to check if local ground 30-100mHz Z BLRMS motion is low than EQ_DOWN_RMS_THRESHOLD
    below_z_RMS_threshold_DOWN = True

# Check for every STS (IX, IY, EX, EY) and make boolean = True if condition is met for ALL
    for chamber in blrms_chambers:
        for dof in ['X','Y','Z']:
            below_z_RMS_threshold_DOWN = below_z_RMS_threshold_DOWN and (ezca['ISI-GND_STS_' + chamber + '_' + dof + '_BLRMS_30M_100M'] < EQ_DOWN_RMS_THRESHOLD)

    below_z_RMS_threshold_DOWN = below_z_RMS_threshold_DOWN and (ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'] < PEAKMON_THRESHOLD)    

    return below_z_RMS_threshold_DOWN

def has_earthquake_passed_DOWN():
# Define a boolean to check if local ground 30-100mHz Z BLRMS motion is low than EQ_DOWN_RMS_THRESHOLD
    below_z_RMS_threshold_DOWN = True

 # Check for every STS (IX, IY, EX, EY) and make boolean = True if condition is met for ALL
    for chamber in ['ITM','ETM']:
        for dof in ['X','Y']:
            below_z_RMS_threshold_DOWN = below_z_RMS_threshold_DOWN and (ezca['ISI-GND_STS_' + chamber + dof + '_' + dof + '_BLRMS_30M_100M'] < EQ_DOWN_RMS_THRESHOLD)

    below_z_RMS_threshold_DOWN = below_z_RMS_threshold_DOWN and (ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'] < PEAKMON_THRESHOLD)    

    return below_z_RMS_threshold_DOWN

def any_isi_in_EQ_mode():
    for dof in ['X','Y','Z']:
        if ezca['SEI-CS_SENSCOR_' + dof + '_FADE_CUR_CHAN_MON'] == 5:
                notify('Central sensor correction already in EQ mode. Check EQ control MEDM screen!')
                return True
    for chamber in fc_ham_chambers:
        for dof in ['X','Y','Z']:
            if ezca['ISI-' + chamber + '_SENSCOR_' + dof + '_FADE_CUR_CHAN_MON'] == 5:
                notify('Sensor correction already in EQ mode. Check EQ control MEDM screen!')
                return True
    for chamber in end_bsc_chambers:
        for dof in ['X','Y','Z']:
            if ezca['ISI-' + chamber + '_ST1_SENSCOR_' + dof + '_FADE_CUR_CHAN_MON'] == 5:
                notify('Sensor correction already in EQ mode. Check EQ control MEDM screen!')
                return True
    return False


def is_picket_passing_threshold():
    picket_count = 0
    for picket_chan in range(1,6):
        picket_motion_max = ezca['SEI-USGS_STATION_0' + str(picket_chan) + '_MAX']
        if picket_motion_max >= PICKET_THRESHOLD:
            picket_count = picket_count + 1
        if picket_count >= 2:
            log('minimum of ' + str(picket_count) + ' Picket channels just passed 400um/s mark')
            return True
    return False

#########################################
### CLASSES

class INIT(GuardState):
    index = 0
    def main(self):
        return True


class IDLE(GuardState):
    index = 1
    def main(self):
        return True
    @linear_model_velocity
    def run(self):
        return True


class EQ_MONITOR(GuardState):
    index=1000
    @lock_loss_dec
    @linear_model_velocity
    def run(self):
        #eq_alert, seismon = monitor_seismon_new() #for LM prediction version instead of SEISMON
        eq_alert, seismon = monitor_seismon()
        peakmon = ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON']
        if eq_alert and ezca['GRD-ISC_LOCK_STATE_N'] == low_noise:
            log('***********\n')
            log('Get ready! earthquake is arriving R-wave predicted at: ' + str(gpstime.gpsnow() + ezca['SEI-SEISMON_LLO_R35_ARRIVALTIME_TOTAL_SECS_' + str(seismon)]) + '\n')
            log('mag ' + str(ezca['SEI-SEISMON_EQ_MAGNITUDE_MMS_'+ str(seismon)]) + '\n')
            log('dist ' + str(ezca['SEI-SEISMON_EQ_LLO_DISTANCE_M_'+ str(seismon)]) + '\n')
            log('depth ' + str(ezca['SEI-SEISMON_EQ_DEPTH_KM_'+ str(seismon)]) + '\n')
            log('velocity ' + str(ezca['SEI-SEISMON_LLO_R35_VELOCITY_MPS_' + str(seismon)]) + '\n')
            log('lat ' + str(ezca['SEI-SEISMON_EQ_LATITUDE_DEG_' + str(seismon)]) + '\n')
            log('long ' + str(ezca['SEI-SEISMON_EQ_LONGITUDE_DEG_' + str(seismon)]) + '\n')
            return 'EARTHQUAKE_ALERT'
#this will check if we got a big earthquake but did not get a seismon alert and will request engagment immidiately
        #elif ((ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'] >= 4*PEAKMON_THRESHOLD) or (ezca['ISI-USGS_NETWORK_PEAK'] >= PICKET_FENCE_THRESHOLD) and ezca['ISI-USGS_NETWORK_PEAK'] < PICKET_FENCE_GLITCH) and (not eq_alert):
        #elif (ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'] >= 4*PEAKMON_THRESHOLD) and (not eq_alert):
        elif ((peakmon >= PEAKMON_THRESHOLD) or is_picket_passing_threshold()) and (not eq_alert):
            notify('Earthquake here! No warning! Engage EQ mode immidiately') 
            nodes['SEI_CONFIG'] = 'EARTHQUAKE_ON'
            return 'EARTHQUAKE_HERE'
#--------------------
        if ezca['GRD-ISC_LOCK_STATE_N'] == low_noise:
            if in_earthquake_mode():
                notify('Low Noise - EQ mode')
            else:
                notify('Low Noise - Nominal mode')
        return True


class EARTHQUAKE_ALERT(GuardState):
    index = 20

    def main(self):
       log('prepare EQ mode\n')
       nodes['SEI_CONFIG'] = 'PREP_EQ_MODE'
    @lock_loss_dec
    @linear_model_velocity
    def run(self):
        notify('Prep EQ mode')
        if is_big_earthquake_arriving() or is_picket_passing_threshold():
            notify('Earthquake here - Engaging EQ mode')         
            return 'EARTHQUAKE_HERE'
        #eq_alert, seismon = monitor_seismon_new(max_wait_time=-60) # check if time arrival has passed 60 mins but we are still stuck at EQ alert, then more likely peakmon did not and will not pass threshold - return EQ_PASSED #for LM instead of SEISMON
        eq_alert, seismon = monitor_seismon()
        if not eq_alert:
            return 'EARTHQUAKE_PASSED'
        return True


class EARTHQUAKE_HERE(GuardState):
    index = 26
    def main(self):
        log('***********\n')
        log('Engage EQ mode, Earthquake is here at time: ' + str(gpstime.gpsnow())+'\n')
        log('Ground peak velocity in Z: ' + str(ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'])+'\n')
        log('Ground RMS velocity in Z: ' + str(ezca['ISI-GND_STS_ETMY_Z_BLRMS_30M_100M'])+'\n')
        log('Ground RMS velocity, horizontal: ' + str((ezca['ISI-GND_STS_ETMX_X_BLRMS_30M_100M']+ezca['ISI-GND_STS_ETMY_Y_BLRMS_30M_100M'])/2))
        nodes['SEI_CONFIG'] = 'EARTHQUAKE_ON'
        self.timer['wait'] = earthquake_timer
    @lock_loss_dec
    @linear_model_velocity
    def run(self):
        notify('Low Noise - EQ mode')
        #Arrived and done is to ensure we have finished transitioning to the EARTHQUAKE_ON state
        if has_earthquake_passed() and self.timer['wait'] and nodes['SEI_CONFIG'].arrived and nodes['SEI_CONFIG'].done:
            notify('Earthquake passed')          
            return 'EARTHQUAKE_PASSED'
        return True


class EARTHQUAKE_PASSED(GuardState):
    index = 31
    def main(self):
        log('***********\n')
        log('Engage nominal, Earthquake has passed: ' + str(gpstime.gpsnow())+'\n')
        notify('Earthquake passed. Engage Nominal mode')
        nodes['SEI_CONFIG'] = 'EARTHQUAKE_OFF'
        time.sleep(60)
        nodes['SEI_CONFIG'] = 'ALL_ISO'
        return False #'EQ_MONITOR'
    @lock_loss_dec
    @linear_model_velocity
    def run(self):
        if nodes['SEI_CONFIG'].arrived and nodes['SEI_CONFIG'].done:
            return 'EQ_MONITOR'
        return False


class DOWN(GuardState):
    index = 50
    def main(self):
        log('Oh No it is unlocked! Lock loss occured at ' + str(gpstime.gpsnow())+'\n')
        log('RMS ground velocity, vertical: ' + str(ezca['ISI-GND_STS_ETMY_Z_BLRMS_30M_100M']))
        log('PEAK ground velocity, vertical: ' + str(ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON']))       
        log('RMS ground velocity, horizontal: ' + str((ezca['ISI-GND_STS_ETMX_X_BLRMS_30M_100M']+ezca['ISI-GND_STS_ETMY_Y_BLRMS_30M_100M'])/2))
        self.AD_flag = False
        self.seiconf_flag =True
        self.timer['EQ_OFF'] = 0

    @linear_model_velocity
    def run(self):
        if self.AD_flag:
            notify('Motion too high. Switch all ISI to DAMPED')
        if (not self.AD_flag) and (ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'] >= DAMPED_THRESHOLD):
            log('BIG earthquake. Switching all ISI to DAMPED')            
            for chamber in ham_chambers+bsc_chambers+fc_ham_chambers:
                if chamber == 'HAM1':
                    pass
                else:
                    ezca['GRD-SEI_'+chamber+'_REQUEST'] = 'DAMPED'
                    time.sleep(10)
                    ezca['GRD-SEI_BS_REQUEST'] = 'DAMPED'  
            self.AD_flag = True
        if (any_isi_in_EQ_mode() and self.timer['EQ_OFF']):
            log('Found EQ mode on.  Turning EQ filters off')
            time.sleep(70) #make sure that EQ mode is done transitioning if triggered right before lockloss
            nodes['SEI_CONFIG'] = 'IDLE'
            time.sleep(1)
            nodes['SEI_CONFIG'] = 'EARTHQUAKE_OFF'
            self.timer['EQ_OFF'] = 70
            if not self.AD_flag:
                time.sleep(70)
                log('EARTHQUAKE_OFF transition is done. Request SEI_CONFIG state to ALL_ISO')
                nodes['SEI_CONFIG'] = 'ALL_ISO'
        elif has_earthquake_passed_DOWN():
            log('EQ has passed. Safe to return to: Nominal mode')
            notify('Safe to return to: Nominal mode')
            if self.AD_flag:
                notify('ISI is damped. EQ passed. Requesting Isolated now.')
                nodes['SEI_CONFIG'] = 'RESET_ALL_WD'               
                time.sleep(15)
                nodes['SEI_CONFIG'] = 'ALL_ISO'
                self.AD_flag = False
                time.sleep(0.5)
        if ezca['GRD-ISC_LOCK_STATE_N'] == low_noise:
            log('Returning SEI_EQ to EQ_MONITOR')
            nodes['SEI_CONFIG'] = 'ALL_ISO' 
            return 'EQ_MONITOR'
        return True
        

#########################################

edges = [
    ('INIT','IDLE'),
    ('IDLE','EQ_MONITOR'),
    ('EQ_MONITOR','IDLE'),
#    ('DOWN','EQ_MONITOR'),
    ]
